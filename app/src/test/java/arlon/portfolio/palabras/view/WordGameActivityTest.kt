package arlon.portfolio.palabras.view

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import arlon.portfolio.palabras.R
import arlon.portfolio.palabras.model.size
import arlon.portfolio.palabras.viewmodel.TestViewModel
import kotlinx.android.synthetic.main.activity_word_game.*
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.mockito.Mockito.*

@RunWith(AndroidJUnit4::class)
class WordGameActivityTest : KoinTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var activityRule: ActivityScenarioRule<WordGameActivity> =
        ActivityScenarioRule(WordGameActivity::class.java)

    private val context: WordGameActivity = mock(WordGameActivity::class.java)
    private lateinit var viewModel: TestViewModel

    @Before
    fun setup() {
        `when`(context.resources).thenReturn(mock(Resources::class.java))
        viewModel = TestViewModel(context, TestCoroutineScope())
    }

    @Test
    fun `check that words have been loaded correctly`() {
        val scenario = activityRule.scenario
        scenario.moveToState(Lifecycle.State.CREATED).onActivity { activity ->
            with(activity.fvWords.adapter) {
                assertEquals(count, viewModel.wordList.size())
            }
        }
    }

    @Test
    fun `checks that words have been loaded correctly`() {
        val scenario = activityRule.scenario
        scenario.moveToState(Lifecycle.State.CREATED).onActivity { activity ->
            with(activity.fvWords.adapter) {
                assertEquals(count, viewModel.wordList.size())
            }
        }
    }

    @Test
    fun `check that bottom button is responsive`() {
        activityRule.scenario.moveToState(Lifecycle.State.CREATED).onActivity { activity ->
            onView(withId(R.id.cvCorrect)).perform(click())
            verify(context, times(1)).answerQuiz(null)
            assertEquals(activity.tvCorrect.text.toString(), viewModel.correctCount.value)
        }
    }

    @Test
    fun `check that bottom button is responsive 2`() {
        activityRule.scenario.moveToState(Lifecycle.State.CREATED).onActivity { activity ->
            onView(withId(R.id.cvWrong)).perform(click())
            verify(context, times(1)).answerQuiz(null)
            assertEquals(activity.tvWrong.text.toString(), viewModel.wrongCount.value)
        }
    }

    @After
    fun tearDown() {
    }
}