package arlon.portfolio.palabras.viewmodel

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import org.json.JSONObject
import java.util.*

class TestViewModel(context: Context, scope: CoroutineScope) : WordViewModel(context, scope) {

    override fun readJSONContent(): JSONObject? {
        val inputStream = javaClass.getResourceAsStream("/raw/words.json")
        val inputString = Scanner(inputStream).useDelimiter("\\A").next()
        return getObject(inputString)
    }
}