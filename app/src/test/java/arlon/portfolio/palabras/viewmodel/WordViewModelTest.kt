package arlon.portfolio.palabras.viewmodel

import android.content.Context
import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arlon.portfolio.palabras.R
import arlon.portfolio.palabras.model.size
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.*

class WordViewModelTest : KoinTest {

    @Rule
    @JvmField
    var instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private val testScope = TestCoroutineScope()
    private lateinit var viewModel: TestViewModel

    @Before
    fun setup() {
        val context: Context = mock(Context::class.java)
        `when`(context.resources).thenReturn(mock(Resources::class.java))
        viewModel = TestViewModel(context, testScope)
    }

    /*
    * Condition: At launch or upon moving to a different question
    *
    * Expected outcome:
    * [viewModel.currentWord] should be set to the expected value specified by the generated random
    * number
    * [viewModel.wordCount] should decrement by a unit
    * [viewModel.reaction] should be set to {R.drawable.ic_face_none}
    * */
    @Test
    fun `ensure that current word gets set correctly`() {
        val randNum = Random().nextInt(viewModel.wordList.size())
        val expectedWord = viewModel.wordList.wordPairs[randNum]
        val totalCount = viewModel.attemptCount.value ?: 0
        viewModel.setCurrentWord(randNum)
        assertEquals(expectedWord, viewModel.currentWord.value)
        assertEquals(randNum, viewModel.wordList.positionOf(expectedWord))
        assertEquals((totalCount - 1), viewModel.attemptCount.value)
        assertEquals(viewModel.reaction.value, R.drawable.ic_face_none)
    }

    /*
    * Condition: The user chooses to skip the current question
    *
    * Expected outcome:
    * [viewModel.wrongCount] should increment by a unit
    * [viewModel.reaction] should be set to {R.drawable.ic_face_sad}
    * */
    @Test
    fun `ensure that the user can successfully skip the current word`() {
        val randNum = Random().nextInt(viewModel.wordList.size())
        viewModel.setCurrentWord(randNum)
        val wrongCount = viewModel.wrongCount.value ?: 0
        viewModel.skipCurrentWord()
        assertEquals((wrongCount + 1), viewModel.wrongCount.value)
        assertEquals(viewModel.reaction.value, R.drawable.ic_face_sad)
    }

    /*
    * Condition: The user chooses the right answer
    *
    * Expected outcome:
    * [viewModel.wordCount] should decrement by a unit
    * [viewModel.correctCount] should increment by a unit
    * [viewModel.reaction] should be set to {R.drawable.ic_face_smile}
    * */
    @Test
    fun `ensure that the user's successful attempt is recorded`() {
        val randNum = Random().nextInt(viewModel.wordList.size())
        viewModel.setCurrentWord(randNum)
        viewModel.suggestedTrans.value = viewModel.currentWord.value?.text_spa
        val correctCount = viewModel.correctCount.value ?: 0
        viewModel.answerQuiz(true)
        assertEquals((correctCount + 1), viewModel.correctCount.value)
        assertEquals(viewModel.reaction.value, R.drawable.ic_face_smile)
    }

    /*
    * Condition: The user chooses the wrong answer
    *
    * Expected outcome:
    * [viewModel.wordCount] should decrement by a unit
    * [viewModel.wrongCount] should increment by a unit
    * [viewModel.reaction] should be set to {R.drawable.ic_face_sad}
    * */
    @Test
    fun `ensure that the user's failed attempt is recorded`() {
        val randNum = Random().nextInt(viewModel.wordList.size())
        viewModel.setCurrentWord(randNum)
        viewModel.suggestedTrans.value =
            viewModel.wordList.wordPairs[viewModel.wordList.size() - randNum].text_spa
        val wrongCount = viewModel.wrongCount.value ?: 0
        viewModel.answerQuiz(true)
        assertEquals((wrongCount + 1), viewModel.wrongCount.value)
        assertEquals(viewModel.reaction.value, R.drawable.ic_face_sad)
    }
}