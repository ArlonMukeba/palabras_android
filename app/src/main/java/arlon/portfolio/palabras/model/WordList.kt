package arlon.portfolio.palabras.model

import com.google.gson.annotations.SerializedName

data class WordList(

    @SerializedName("words") val wordPairs: List<WordPair>
) {
    fun positionOf(wordPair: WordPair): Int = wordPairs.indexOf(wordPair)
}