package arlon.portfolio.palabras.model

data class WordPair(

    val text_eng: String,
    val text_spa: String
) {
    override fun equals(other: Any?): Boolean = (text_eng == (other as WordPair).text_eng &&
            text_spa == other.text_spa)
}