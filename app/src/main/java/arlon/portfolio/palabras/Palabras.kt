package arlon.portfolio.palabras

import android.app.Application
import arlon.portfolio.palabras.viewmodel.WordViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class Palabras : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@Palabras)

            modules(
                listOf(
                    appModule
                )
            )
        }
    }

    private val appModule = module {
        viewModel { WordViewModel(applicationContext) }
    }
}