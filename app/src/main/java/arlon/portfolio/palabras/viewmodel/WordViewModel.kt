package arlon.portfolio.palabras.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arlon.portfolio.palabras.R
import arlon.portfolio.palabras.model.WordList
import arlon.portfolio.palabras.model.WordPair
import arlon.portfolio.palabras.model.size
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.koin.core.KoinComponent
import java.util.*
import kotlin.random.Random

open class WordViewModel(private val context: Context, var scope: CoroutineScope? = null) :
    ViewModel(), KoinComponent {

    lateinit var wordList: WordList
    private var answerList = mutableListOf<String>()
    var suggestedTrans: MutableLiveData<String> = MutableLiveData("")
    var attemptCount: MutableLiveData<Int> = MutableLiveData(0)
    var correctCount: MutableLiveData<Int> = MutableLiveData(0)
    var wrongCount: MutableLiveData<Int> = MutableLiveData(0)
    var currentWord: MutableLiveData<WordPair> = MutableLiveData(WordPair("", ""))
    var reaction: MutableLiveData<Int> = MutableLiveData(R.drawable.ic_face_none)

    init {
        if (scope == null) {
            scope = viewModelScope
        }

        scope?.let {
            it.launch {
                getWordList()
            }
        }
    }

    private fun getWordList() {
        wordList = Gson().fromJson(
            readJSONContent().toString(),
            WordList::class.java
        )

        wordList.let { list ->
            attemptCount.value = list.wordPairs.size
            answerList.apply {
                list.wordPairs.forEach { add(it.text_spa) }
            }
        }
    }

    fun setCurrentWord(position: Int) {
        attemptCount.value = (attemptCount.value ?: 0) - 1
        currentWord.value = wordList.wordPairs.get(position)
        suggestedTrans.value = answerList[Random.nextInt(
            position, answerList.size.coerceAtMost(
                position + 4
            )
        )]
        reaction.value = R.drawable.ic_face_none
    }

    fun skipCurrentWord() {
        suggestedTrans.value = currentWord.value?.text_spa
        wrongAnswer()
    }

    fun answerQuiz(correct: Boolean) {
        if ((currentWord.value?.text_spa == suggestedTrans.value) == correct) correctAnswer() else wrongAnswer()
        suggestedTrans.value = currentWord.value?.text_spa
    }

    private fun correctAnswer() {
        reaction.value = R.drawable.ic_face_smile
        correctCount.value = (correctCount.value ?: 0) + 1
    }

    private fun wrongAnswer() {
        reaction.value = R.drawable.ic_face_sad
        wrongCount.value = (wrongCount.value ?: 0) + 1
    }

    fun resetGame() {
        attemptCount.value = wordList.size()
        correctCount.value = 0
        wrongCount.value = 0
        reaction.value = R.drawable.ic_face_none
        setCurrentWord(0)
    }

    open fun readJSONContent(): JSONObject? {
        val inputStream = context.resources.openRawResource(R.raw.words)
        val inputString = Scanner(inputStream).useDelimiter("\\A").next()
        return getObject(inputString)
    }

    open fun getObject(jsonString: String): JSONObject? {
        return try {
            JSONObject(jsonString)
        } catch (e: java.lang.Exception) {
            JSONObject()
        }
    }
}