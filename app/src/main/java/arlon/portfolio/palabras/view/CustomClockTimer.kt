package arlon.portfolio.palabras.view

import android.R.attr.radius
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

class CustomClockTimer(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var angleList = mutableListOf<Pair<Float, Float>>()
    private var active = false
    private var counter = 0
    var countDown = DEFAULT_COUNT_DOWN

    private var drawPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        strokeCap = Paint.Cap.ROUND
        strokeWidth = 3.0f
        color = context.getColor(android.R.color.white)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let { c ->
            drawLine(c, 0)
            drawLine(c, counter)
        }
    }

    private fun drawLine(canvas: Canvas, position: Int, cxy: Int = (measuredWidth / 2)) {
        angleList[position].let {
            canvas.drawLine(cxy.toFloat(), cxy.toFloat(), it.first, it.second, drawPaint)
        }
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        super.onMeasure(widthSpec, heightSpec)
        createPairs()
        setMeasuredDimension(widthSpec, heightSpec)
    }

    private fun createPairs() {
        for (angle in 0..360 step (360 / countDown)) {
            val displacedAngle = angle - 90
            val x: Float = cos(displacedAngle * PI / 180).toFloat() * radius
            val y: Float = sin(displacedAngle * PI / 180).toFloat() * radius
            angleList.add(Pair(x, y))
        }
    }

    fun startCounter() {
        active = true
        proceedCounting()
    }

    private val runnable = Runnable {
        if (counter < countDown) {
            invalidate()

            if (active) {
                ++counter
                proceedCounting()
            }
        }
    }

    private fun proceedCounting() {
        Handler().postDelayed(runnable, 1000)
    }

    fun stopCounter() {
        active = false
        counter = 0
    }

    companion object {
        const val DEFAULT_COUNT_DOWN = 20
    }
}