package arlon.portfolio.palabras.view

import android.os.Bundle
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import arlon.portfolio.palabras.R
import arlon.portfolio.palabras.databinding.ActivityWordGameBinding
import arlon.portfolio.palabras.viewmodel.WordViewModel
import kotlinx.android.synthetic.main.activity_word_game.*
import org.koin.android.viewmodel.ext.android.viewModel

class WordGameActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    private val viewModel: WordViewModel by viewModel()
    private lateinit var binding: ActivityWordGameBinding
    private var translAnimation: Animation? = null
    private var timerAnimation: Animation? = null
    private var wasAnswered = false
    private var wasSkipped = false
    private var wasPaused = false
    private lateinit var tts: TextToSpeech

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_word_game)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.fvWords.adapter = WordsAdapter(viewModel)

        binding.fvWords.setOnFlipListener { _, position, _ ->
            if (wasPaused) return@setOnFlipListener
            Handler().postDelayed({ resumeGame(position) }, 500)
        }

        tts = TextToSpeech(this, this)

        setSupportActionBar(binding.toolbar)
        promptStartGame()
    }

    private fun promptStartGame() {
        CustomDlgScreen(
            contentResId = R.string.start_game_dlg_content,
            dialogListener = object : CustomDlgScreen.DialogListener() {
                override fun onPositive() {
                    resumeGame()
                }

                override fun onNegative() {
                    cvWordAnswer.visibility = View.GONE
                    toggleAllActions(false)
                }
            }
        ).showDialog(supportFragmentManager)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu?.findItem(R.id.menu_start_game)?.isVisible = wasPaused
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_exit_app -> {
                promptExitApp()
            }
            R.id.menu_reset_game -> {
                resetGame()
            }
            R.id.menu_start_game -> {
                resumeGame()
            }
        }
        return true
    }

    private fun resumeGame(position: Int = 0) {
        wasPaused = false
        wasAnswered = false
        viewModel.setCurrentWord(position)
        binding.cctClockTimer.startCounter()
        toggleAllActions()
        animateTranslation()
        invalidateOptionsMenu()
    }

    private fun resetGame() {
        wasSkipped = false
        wasAnswered = true
        wasPaused = true
        viewModel.resetGame()
        binding.fvWords.flipTo(0)
        binding.cctClockTimer.stopCounter()
        binding.cvWordAnswer.visibility = View.GONE
        translAnimation?.cancel()
        timerAnimation?.cancel()
        toggleAllActions(false)
        invalidateOptionsMenu()
    }

    private fun promptExitApp() {
        CustomDlgScreen(
            contentResId = R.string.exit_app_dlg_content,
            dialogListener = object : CustomDlgScreen.DialogListener() {
                override fun onPositive() {
                    finish()
                }

                override fun onNegative() {
                }
            }).showDialog(
            supportFragmentManager
        )
    }

    fun skipCurrentWord(view: View) {
        wasSkipped = true
        wasAnswered = false
        translAnimation?.cancel()
    }

    fun answerQuiz(view: View?) {
        wasAnswered = true
        translAnimation?.cancel()
        viewModel.answerQuiz(view?.id == R.id.cvCorrect)
    }

    private fun animateTranslation() {
        translAnimation = AnimationUtils.loadAnimation(this, R.anim.translation_anim)
        translAnimation?.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {
                binding.cvWordAnswer.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation?) {
                toggleAllActions(false)
                binding.cctClockTimer.stopCounter()

                if (wasSkipped) {
                    viewModel.skipCurrentWord()
                    pauseBeforeResuming()
                } else if (!wasAnswered) {
                    animateTimer()
                } else {
                    pauseBeforeResuming()
                }
            }
        }
        )
        binding.cvWordAnswer.startAnimation(translAnimation)
    }

    private fun toggleAllActions(state: Boolean = true) {
        binding.cvCorrect.isEnabled = state
        binding.cvWrong.isEnabled = state
        binding.tvSkip.isEnabled = state
        binding.ivSpeak.isEnabled = state
    }

    private fun pauseBeforeResuming() {
        if (wasPaused) return
        Handler().postDelayed({
            binding.cvWordAnswer.visibility = View.GONE
            binding.fvWords.smoothFlipBy(1)
        }, ANSWER_DISPLAY_WAITING_PERIOD)
    }

    private fun animateTimer() {
        timerAnimation = AnimationUtils.loadAnimation(this, R.anim.timer_anim)
        timerAnimation?.repeatCount = TIMER_ANIMATION_REPEAT_COUNT
        timerAnimation?.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                viewModel.skipCurrentWord()
                pauseBeforeResuming()
            }
        }
        )
        binding.ivClockTimer.startAnimation(timerAnimation)
    }

    companion object {
        const val ANSWER_DISPLAY_WAITING_PERIOD: Long = 3500
        const val TIMER_ANIMATION_REPEAT_COUNT: Int = 100000
    }

    override fun onInit(status: Int) {
        binding.ivSpeak.visibility = when (status) {
            TextToSpeech.SUCCESS -> {
                when (tts.isLanguageAvailable(tts.voice.locale)) {
                    TextToSpeech.LANG_MISSING_DATA -> View.INVISIBLE
                    TextToSpeech.LANG_NOT_SUPPORTED -> View.INVISIBLE
                    else -> View.VISIBLE
                }
            }
            else -> View.INVISIBLE
        }
    }

    fun speak(view: View) {
        tts.speak(viewModel.currentWord.value?.text_eng, TextToSpeech.QUEUE_FLUSH, null, "")
    }

    override fun onBackPressed() {
        promptExitApp()
    }
}