package arlon.portfolio.palabras.view

import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import arlon.portfolio.palabras.R
import arlon.portfolio.palabras.model.WordPair
import arlon.portfolio.palabras.viewmodel.WordViewModel
import kotlinx.android.synthetic.main.word_item_layout.view.*

class WordsAdapter(viewModel: WordViewModel) : BaseAdapter() {

    private val words: List<WordPair>? = viewModel.wordList.wordPairs

    override fun registerDataSetObserver(observer: DataSetObserver?) {}
    override fun unregisterDataSetObserver(observer: DataSetObserver?) {}
    override fun getCount(): Int = words?.size ?: 0
    override fun getItem(position: Int): Any? = words?.get(position)
    override fun getItemId(position: Int): Long = position.toLong()
    override fun hasStableIds(): Boolean = true

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder: WordViewHolder
        val view: View

        if (convertView == null) {
            holder = WordViewHolder()
            view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.word_item_layout, parent, false)
            holder.text = view.tvWord as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as WordViewHolder
        }

        holder.text?.text = (getItem(position) as WordPair).text_eng

        return view
    }

    override fun getItemViewType(position: Int): Int = position
    override fun getViewTypeCount(): Int = words?.size ?: 0
    override fun isEmpty(): Boolean = false
    override fun areAllItemsEnabled(): Boolean = true
    override fun isEnabled(position: Int): Boolean = false
}

class WordViewHolder {
    var text: TextView? = null
}