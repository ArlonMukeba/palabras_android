package arlon.portfolio.palabras.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import arlon.portfolio.palabras.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        animateLogo()
    }

    private fun animateLogo() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.logo_anim)
        animation.fillAfter = true
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                moveToPalettePicker()
            }
        }
        )
        cvLogo.startAnimation(animation)
    }

    private fun moveToPalettePicker() {
        tvLabel.isVisible = true

        val runnable = Runnable {
            Intent(this, WordGameActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }.let(this::startActivity)
        }

        Handler().postDelayed(runnable, 1000)
    }
}