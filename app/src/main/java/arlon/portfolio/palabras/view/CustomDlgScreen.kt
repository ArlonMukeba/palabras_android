package arlon.portfolio.palabras.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import arlon.portfolio.palabras.R
import kotlinx.android.synthetic.main.activity_dialog.*

class CustomDlgScreen(
    private val contentResId: Int = R.string.start_game_dlg_content,
    private val btnLblResId: Int = R.string.dlg_btn_lbl_okay,
    private val btnResIds: List<Int> = listOf(
        R.drawable.ic_action_game,
        R.drawable.ic_action_close
    ),
    private val dialogListener: DialogListener
) : DialogFragment() {

    override
    fun onCreateView(
        layoutInflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return layoutInflater.inflate(R.layout.activity_dialog, container, false)
    }

    override
    fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val root = LinearLayout(activity)

        root.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        val dialog = Dialog(requireContext(), android.R.style.Theme_Material_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window!!.setBackgroundDrawableResource(R.color.blackOverlay)

        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        return dialog
    }

    override
    fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tvContent.text = context?.getString(contentResId)
        tvBtnPos.text = context?.getString(btnLblResId)
        tvBtnPos.setCompoundDrawablesWithIntrinsicBounds(btnResIds[0], 0, 0, 0)
        tvBtnNeg.setCompoundDrawablesWithIntrinsicBounds(btnResIds[1], 0, 0, 0)

        cvBtnPos.setOnClickListener { startGame() }
        cvBtnNeg.setOnClickListener { closeDlg() }
        ivDlgClose.setOnClickListener { closeDlg() }
    }

    private fun startGame() {
        dialog?.dismiss()
        dialogListener.onPositive()
    }

    private fun closeDlg() {
        dialog?.dismiss()
        dialogListener.onNegative()
    }

    override
    fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    fun showDialog(fm: FragmentManager) {
        fm?.let {
            show(fm, "TestFragment")
        }
    }

    abstract class DialogListener {
        abstract fun onPositive()
        abstract fun onNegative()
    }
}