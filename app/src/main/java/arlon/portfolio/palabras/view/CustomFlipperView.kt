package arlon.portfolio.palabras.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import se.emilsjolander.flipview.FlipView

class CustomFlipperView(context: Context, attrs: AttributeSet) : FlipView(context, attrs) {
    override fun onTouchEvent(ev: MotionEvent?): Boolean = true
}